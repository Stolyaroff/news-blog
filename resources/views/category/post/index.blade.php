@extends ('layouts.main')

@section('content')
<main class="blog">
    <div class="container">
        <h1 class="edica-page-title colortext-gray" data-aos="fade-up">{{ $category->title }}</h1>
        <section class="featured-posts-section">
            <div class="row">
                @foreach($posts as $post)
                <div class="col-md-4 fetured-post blog-post mb-5" data-aos="fade-up">
                    <div class="blog-post-thumbnail-wrapper">
                        <img src="{{ asset('storage/' . $post->preview_image) }}" alt="blog post">
                    </div>
                    <div class="d-flex justify-content-between">
                        <p class="blog-post-category">{{ $post->category->title }}</p>
                        @auth()
                            <form action="{{ route('post.like.store', $post->id) }}" method="post">
                                @csrf
                                {{ $post->liked_users_count }}
                                <button type="submit" class="border-0 bg-transparent">

                                    <i class="fa{{ auth()->user()->likedPosts->contains($post->id)?'s':'r' }} fa-thumbs-up"></i>
                                </button>
                            </form>
                        @endauth
                    @guest()
                            <div class="">
                                {{ $post->liked_users_count }}
                                <i class="far fa-thumbs-up"></i>
                            </div>
                        @endguest
                    </div>
                    <a href="{{ route('post.show', $post->id) }}" class="blog-post-permalink">
                        <h6 class="blog-post-title">{{ $post->title }}</h6>
                    </a>
                </div>
                @endforeach
                <div class="row mb-5 mx-auto">
                    <div class="">
                        {{ $posts->links() }}
                    </div>
                </div>
            </div>
        </section>
    </div>

</main>
@endsection

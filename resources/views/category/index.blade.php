@extends ('layouts.main')

@section('content')
<main class="blog">
    <div class="container">
        <h1 class="edica-page-title colortext-gray" data-aos="fade-up">Категории</h1>
        <section class="featured-posts-section">
            <ul class="list-group list-group-flush">
                @foreach($categories as $category)
                    <li class="list-group-item">
                        <h2>
                            <a class="nav-link colortext-gray" href="{{ route('category.post.index', $category->id) }}">{{$category->title}}
                            </a>
                        </h2>
                    </li>
                @endforeach
            </ul>
        </section>
    </div>
</main>
@endsection

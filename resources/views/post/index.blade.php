@extends ('layouts.main')

@section('content')
<main class="blog">
    <div class="container">
        <h1 class="edica-page-title colortext-gray" data-aos="fade-up">Статьи</h1>
        <section class="featured-posts-section">
            <div class="row">
                @foreach($posts as $post)
                <div class="col-md-4 fetured-post blog-post mb-5" data-aos="fade-up">
                    <div class="blog-post-thumbnail-wrapper">
                        <img src="{{ 'storage/' . $post->preview_image }}" alt="blog post">
                    </div>
                    <div class="d-flex justify-content-between">
                        <p class="blog-post-category">{{ $post->category->title }}</p>
                        @auth()
                            <form action="{{ route('post.like.store', $post->id) }}" method="post">
                                @csrf
                                {{ $post->liked_users_count }}
                                <button type="submit" class="border-0 bg-transparent">

                                    <i class="fa{{ auth()->user()->likedPosts->contains($post->id)?'s':'r' }} fa-thumbs-up"></i>
                                </button>
                            </form>
                        @endauth
                    @guest()
                            <div class="">
                                {{ $post->liked_users_count }}
                                <i class="far fa-thumbs-up"></i>
                            </div>
                        @endguest
                    </div>
                    <a href="{{ route('post.show', $post->id) }}" class="blog-post-permalink">
                        <h6 class="blog-post-title">{{ $post->title }}</h6>
                    </a>
                </div>
                @endforeach
            </div>
            <div class="row mb-5">
                <div class="mx-auto">
                    {{ $posts->links() }}
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-md-8 border-right border-1">
                <section>
                    <h5 class="widget-title colortext-gray" data-aos="fade-up">Может заинтересовать...</h5>
                    <div class="row blog-post-row">
                        @foreach($randomPost as $post)
                        <div class="col-md-6 blog-post" data-aos="fade-up">
                            <div class="blog-post-thumbnail-wrapper">
                                <img src="{{ 'storage/' . $post->preview_image }}" alt="blog post">
                            </div>
                            <p class="blog-post-category">{{ $post->category->title }}</p>
                            <a href="{{ route('post.show', $post->id) }}" class="blog-post-permalink">
                                <h6 class="blog-post-title">{{ $post->title }}</h6>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </section>
            </div>
            <div class="col-md-4 sidebar" data-aos="fade-left">
                <div class="widget widget-post-list">
                    <h5 class="widget-title colortext-gray">Популярные статьи</h5>
                    <ul class="post-list">
                        @foreach($likedPosts as $post)
                        <li class="post">
                            <a href="{{ route('post.show', $post->id) }}" class="post-permalink media">
                                <img src="{{ 'storage/' . $post->preview_image }}" alt="blog post">
                                <div class="media-body">
                                    <h6 class="post-title">{{ $post->title }}</h6>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

</main>
@endsection
